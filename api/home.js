import request from "@/utils/request.js";


// 获取系统信息
export function getSysSettings() {
	return request({
		url: `/jeecg-boot/home/page/getSetUp`,
		method: 'get',
	})
}

// 授权登录
export function getAuth(data) {
	return request({
		url: `/jeecg-boot/weChat/info`,
		method: 'post',
		data
	})
}

// code登录
export function loginByCode(data) {
	return request({
		url: `/jeecg-boot/weChat/code`,
		method: 'post',
		data
	})
}

// 获取环境
export function getSensor(data) {
	return request({
		url: `/jeecg-boot/home/page/getSensor`,
		method: 'get',
		data
	})
}

// 获取天气
export function getWeather(data) {
	return request({
		url: `/jeecg-boot/home/page/getWeather`,
		method: 'get',
	})
}

// 获取摄像头
export function getCamera(data) {
	return request({
		url: `/jeecg-boot/home/page/getCamera`,
		method: 'get',
	})
}

// 获取摄像头
export function login(data) {
	return request({
		url: `/jeecg-boot/weChat/login`,
		method: 'post',
		data
	})
}
